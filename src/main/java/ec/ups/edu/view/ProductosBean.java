package ec.ups.edu.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;


import ec.ups.edu.est.Modelo.Producto;
import ec.ups.edu.est.Modelo.Respuesta;
import ec.ups.edu.est.ObjetosDeNegocio.ClienteON;

@ManagedBean 
@ViewScoped
public class ProductosBean {
	@Inject 
	private ClienteON clienteON;
	
	private Producto pro; 
	private List<Producto> listProductos; 

	@PostConstruct 
	public void iniciar() { 
		pro = new Producto();
	}
	public Producto getPro() {
		return pro;
	}
	public void setPro(Producto pro) {
		this.pro = pro;
	}
	public List<Producto> getListProductos() {
		return listProductos;
	}
	public void setListProductos(List<Producto> listProductos) {
		this.listProductos = listProductos;
	} 
	 
	public void guardarProducto() { 
		clienteON.insertarTransaccion(pro);
	}
	
	public List<Producto> obtenerProductos() { 
		Respuesta respuesta = clienteON.obtenerProductos(); 
		return respuesta.getProductos();
	}
	
}
