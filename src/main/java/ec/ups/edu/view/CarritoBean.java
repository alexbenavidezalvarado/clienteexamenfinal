package ec.ups.edu.view;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import ec.ups.edu.est.Modelo.FacturaCabecera;
import ec.ups.edu.est.Modelo.FacturaDetalle;
import ec.ups.edu.est.Modelo.Producto;
import ec.ups.edu.est.Modelo.Respuesta;
import ec.ups.edu.est.ObjetosDeNegocio.ClienteON;
@ManagedBean
@SessionScoped
public class CarritoBean { 
	@Inject 
	private ClienteON clienteON;
	private FacturaCabecera cabecera; 
	private List<FacturaDetalle> detalles;  
	private int cantidad; 
	private int codigoProducto; 
	private boolean visualizar = false;
	
	@PostConstruct 
	public void Iniciar() {
		cabecera = new FacturaCabecera(); 
		detalles = new ArrayList<FacturaDetalle>();
	}
	
		
	public FacturaCabecera getCabecera() {
		return cabecera;
	}



	public void setCabecera(FacturaCabecera cabecera) {
		this.cabecera = cabecera;
	}


	public List<FacturaDetalle> getDetalles() {
		return detalles;
	}




	public void setDetalles(List<FacturaDetalle> detalles) {
		this.detalles = detalles;
	}


	public int getCantidad() {
		return cantidad;
	}



	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public List<Producto> obtenerProductos() { 
		Respuesta respuesta = clienteON.obtenerProductos(); 
		return respuesta.getProductos();
	}

	
	
	public int getCodigoProducto() {
		return codigoProducto;
	}


	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	
	
	

	public boolean isVisualizar() {
		return visualizar;
	}


	public void setVisualizar(boolean visualizar) {
		this.visualizar = visualizar;
	}


	public void anadirCarrito() {  
		
		FacturaDetalle facturaDetalle = new FacturaDetalle(); 
		facturaDetalle.setCantidad(cantidad);
		Producto p = clienteON.obtenerProducto(codigoProducto); 
		System.out.println(p);
		facturaDetalle.setProducto(p);
		facturaDetalle.setSubtotal(cantidad*p.getPrecio());  
		if(detalles.size()>0) { 
			for(FacturaDetalle detalle : detalles) {  
				if(detalle.getProducto().getCodigoProducto() == p.getCodigoProducto()) { 
					detalle.setCantidad(detalle.getCantidad()+cantidad); 
					detalle.setSubtotal(detalle.getSubtotal()+(detalle.getProducto().getPrecio()*cantidad));
					
				}else {  
					detalles.add(facturaDetalle);
					break;
				}
			} 
		}else { 
			detalles.add(facturaDetalle);
		}
		
		
		
		System.out.println(detalles.size());
		/*for(FacturaDetalle detalle : detalles) { 
			System.out.println(detalle.getProducto().getNombreProducto()); 
			System.out.println(detalle.getCantidad()); 
			System.out.println(detalle.getSubtotal()); 
			System.out.println();
		}*/
	} 
	
	
	public void obtenerFactura() { 
		visualizar = true;
		cabecera = clienteON.obtenerFacturaCabecera(detalles);  
		System.out.println(cabecera);
	}
	
	public void iniciar() { 
		cabecera = new FacturaCabecera(); 
		detalles = new ArrayList<FacturaDetalle>();
	}
	
}
