package ec.ups.edu.est.ObjetosDeNegocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;

import ec.ups.edu.est.Modelo.Carrito;
import ec.ups.edu.est.Modelo.FacturaCabecera;
import ec.ups.edu.est.Modelo.FacturaDetalle;
import ec.ups.edu.est.Modelo.Producto;
import ec.ups.edu.est.Modelo.Respuesta;

@Stateless
public class ClienteON {
	private String WS_OBTENER_PRODUCTOS = "http://localhost:8080/ServidorBenavidezAlex/ws/servicios/obtenerproductos"; 
	private String WS_INSERTAR_PRODUCTO = "http://localhost:8080/ServidorBenavidezAlex/ws/servicios/ingresarproducto";
	private String WS_OBTENER_PRODUCTO = "http://localhost:8080/ServidorBenavidezAlex/ws/servicios/obtenerproducto"; 
	private String WS_OBTENER_FACTURA_CABECERA = "http://localhost:8080/ServidorBenavidezAlex/ws/servicios/obtenerFactura"; 
	private String WS_ANADIR_CARRITO = "http://localhost:8080/ServidorBenavidezAlex/ws/servicios/anadirCarrito";
	
	public Respuesta obtenerProductos(){ 
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_OBTENER_PRODUCTOS);

		Respuesta res  = target.request().get(new GenericType<Respuesta>() {});
		
		client.close();
		
		return res;
	} 
	
	public Respuesta insertarTransaccion(Producto pro) {
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_INSERTAR_PRODUCTO);
		Respuesta respuesta = target.request().post(Entity.json(pro), Respuesta.class);
		return respuesta;
	}  
	
	
	public Producto obtenerProducto(int codigoProducto) { 
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_OBTENER_PRODUCTO).queryParam("codigo", codigoProducto); ;
		Producto pro = target.request().get(new GenericType<Producto>(){});
		return pro;
	} 
	
	public FacturaCabecera obtenerFacturaCabecera(List<FacturaDetalle> detalles) { 
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_OBTENER_FACTURA_CABECERA);
		FacturaCabecera facturaCabecera  = target.request().post(Entity.json(detalles), FacturaCabecera.class);
		client.close();
		return facturaCabecera;
	} 
	
	public List<FacturaDetalle> anadirCarrito(Carrito carrito) { 
		Client client = ClientBuilder.newClient();		
		WebTarget target = client.target(WS_OBTENER_FACTURA_CABECERA);
		List<FacturaDetalle> lst = target.request().post(Entity.json(carrito), List.class);
		client.close();
		return lst;
	}
	
	
}