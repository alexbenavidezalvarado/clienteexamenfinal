package ec.ups.edu.est.Modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

public class FacturaCabecera { 
	private int codigoCabecera; 
	private List<FacturaDetalle> lstDetalles; 
	private double totalFactura;
	public int getCodigoCabecera() {
		return codigoCabecera;
	}
	public void setCodigoCabecera(int codigoCabecera) {
		this.codigoCabecera = codigoCabecera;
	}
	public List<FacturaDetalle> getLstDetalles() {
		return lstDetalles;
	}
	public void setLstDetalles(List<FacturaDetalle> lstDetalles) {
		this.lstDetalles = lstDetalles;
	}
	public double getTotalFactura() {
		return totalFactura;
	}
	public void setTotalFactura(double totalFactura) {
		this.totalFactura = totalFactura;
	}
	@Override
	public String toString() {
		return "FacturaCabecera [codigoCabecera=" + codigoCabecera + ", lstDetalles=" + lstDetalles + ", totalFactura="
				+ totalFactura + "]";
	} 
	
	
	
	

}
