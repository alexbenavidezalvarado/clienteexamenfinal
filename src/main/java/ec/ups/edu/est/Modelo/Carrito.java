package ec.ups.edu.est.Modelo;

import java.util.List;

public class Carrito {
	private List<FacturaDetalle> listDetalles; 
	private int codigoProducto; 
	private int cantidadProducto;
	public List<FacturaDetalle> getListDetalles() {
		return listDetalles;
	}
	public void setListDetalles(List<FacturaDetalle> listDetalles) {
		this.listDetalles = listDetalles;
	}
	public int getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public int getCantidadProducto() {
		return cantidadProducto;
	}
	public void setCantidadProducto(int cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	} 
	
	
}
